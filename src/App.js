import React, { Component } from 'react';

import loader from './images/loader.svg'
import clearButtom from './images/close-icon.svg'

import Gif from './Gif';

const randomChoice = arr => {
  const randIndex = Math.floor(Math.random() * arr.length)
  return arr[randIndex]
}

const Header = ({ clearSearch, hasResults }) => (
  <div className="header grid">
    { hasResults ? (
      <button>
        <img onClick={clearSearch} src={clearButtom} alt="clear"/>
      </button>
    ) : (
      <h1 className="title">Jiffy</h1> 
    )}
  </div>
)

const UserHint = ({ loading, hintText }) => (
  <div className="user-hint">
    { loading ? <img src={loader} className="block mx-auto" alt="loader"/> : hintText }
  </div>
)

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      searchTerm : '',
      hintText: '',
      gifs: []
    }
  }

  // function that searches the giphy api using fetch and put the search term into the query url and then do something with the results.

  searchGiphy = async searchTerm => {
    this.setState({
      loading: true
    })
    // trying or fetch 
    try {
      // using the await keyword to wait four our response to come back
      const response = await fetch(`https://api.giphy.com/v1/gifs/search?api_key=zu7h83Rndo2AKqcToS7Jqtb7bb7t4O5U&q=${searchTerm}&limit=25&offset=0&rating=PG&lang=en`)

      // we convert our raw response into json data
      const { data } = await response.json();

      // chekc if array of results is empty, if it is throw an error to stop the code and handle the error

      if (!data.length) {
        throw `Nothing found for ${searchTerm}`
      }

      // We grab a random result from our images
      const randomGif = randomChoice(data)
      
      this.setState((prevState, props) => ({
        ...prevState,
        // we use our spread to take the previous gifs and spread them out and then add a random gif onto the end
        gifs: [...prevState.gifs, randomGif],
        loading: false,
        hintText: `Hit enter to see more ${searchTerm}`
      }))

    } catch (error) {
        this.setState((prevState, props) => ({
          ...prevState,
          hintText: error,
          loading: false
        }))
    }
  }

  handleChange = event => {
    const { value } = event.target;
    this.setState((prevState, props) => ({
      // we take out our old props and spread them out
      ...prevState,
      // then we overwrite the one we want after
      searchTerm: value,
      hintText: value.length > 2 ? `Hit enter to search ${value}` : ''
    }))
  }

  handleKeyPress = event => {
    const { value } = event.target;
    // when we have 2 or more characters in the search box
    // and whe also pressed enter, we the want to run a search
    if (value.length > 2 && event.key === 'Enter') {
      // calling our searchGiphy function using the search term
      this.searchGiphy(value);
    }
  }

  // Clear the search by reseting the state
  clearSearch = () => {
    this.setState((prevState, props) => ({
      ...prevState,
      searchTerm: '',
      hintText: '',
      gifs: []
    }))
    this.textInput.focus();
  }
  
  render() {

    const { searchTerm, gifs } = this.state;
    const hasResults = gifs.length;
    return (
      <div className="page">
        <Header clearSearch={this.clearSearch} hasResults={hasResults}/>
        <div className="search grid">
          {gifs.map(gif => (
            <Gif { ...gif }/>
          ))}
          <input 
            className="input grid-item" 
            placeholder="Type something" 
            onChange={this.handleChange}
            onKeyPress={this.handleKeyPress}
            value={searchTerm}
            ref={(input) => {this.textInput = input}}
            />
        </div>
        <UserHint {...this.state}/>
      </div>
    );
  }
}

export default App;
